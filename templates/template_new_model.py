"""
NewModel
=========

"""

# ----------------------------------------------
# Required: Replace NewModel by the name of
# the new model class everywhere in the file.
# ----------------------------------------------
import logging
import shutil
import os
import numpy as np
from datetime import timedelta
from .model import Model
from ..common.vector import Vector
from ..common.matrix import Matrix
# ----------------------------------------------
# Required: add an error class in
# ../common.errors.py
# ----------------------------------------------
from ..common.errors import NewModelError

# ----------------------------------------------
# Add the new model in the instanciation method
# instanciate.py
# ----------------------------------------------

# ======================================================


class NewModel(Model):

    logger = logging.getLogger('NewModel')
    logging.Logger.setLevel(logger, logging.INFO)

    # Control vector
    @property 
    def control(self):
        return self._control
    
    @control.setter 
    def control(self, value):
        """
        Dictionary describing the variables to control and their location
        Initialised by Model, updated by Assim.
        It defines the control vector length and the index of each control variables
        e.g. self.control = {'Ks': [1,2,3], 'h': 'all'}
             self.ctl_length = 466  for a mesh of 463 points
             self.first_node = [0, 1, 2, 3, 466]
        """
        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------

        self.ctl_length = 0
        self.first_node = [0]

        if value == {} or self.postproc:
            self._control = None
        else:
            self._control = value
            # ----------------------------------------------
            # Define the length of the control vector
            # for each controlled variable.
            for var in self._control:
                if var == 'b':
                    # Example for GP sampler hyperparameters
                    for i in range(len(self._control[var])):
                        self.ctl_length += len(self.keep_gps[i])
                else:
                    self.ctl_length += len(self._control[var])
            # ----------------------------------------------
                self.first_node.append(self.ctl_length)
        
    # =================================== #
    #               Methods               #
    # =================================== #
    
    def __init__(self, config, prm, statdir='.', wdir='Work', archdir='Archive', member=0,
                 start=None, start0=None, postproc=False):
        """Constructor
            config:       path and name of the file containing the configuration
            prm:          path and name of the file containing the parameters
            statdir:      path to the required files as enumerated in the item ['files'] of the configuration file
                          if None, files are expected to be in the launch directory
            wdir:         working directory
            archdir:      archiving directory
            member:       member id: 0 if no ensemble,
                                     from 1 to Ne if there is an ensemble
            start:        datetime start to carry on the experiment
            start0:       datetime original start of the experiment
            postproc:     instanciation for post-processing purposes
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # Basic initialisation
        Model.__init__(self, config, prm, statdir, wdir, archdir, member, start, start0, postproc)
        self.name = 'NewModel'
        
        # ----------------------------------------------
        # Initialise specific attributes.
        # ----------------------------------------------
        
        # Initialisation of specific attributes
        # self.interpol = {}                  # Dictionary of interpolation information
        #                                     {model_coord_1: { 'ind1': (index, weight),
        #                                     'ind2': (index, weight)}}
        # self.persist = False                # Persistence for forecast on going (bool)

        # Post processing attributes
        if self.postproc:
            # ----------------------------------------------
            # Initialise post processing attributes
            # that will be updated in extract_info().
            # ----------------------------------------------

            # Post processing attributes
            # self.var_ind = None  # Indices for extracting variables from output file
            # self.tim_ind = None  # Indices for extracting time steps from output file
            # self.loc_ind = None  # Indices for extracting location from output file
            pass

        # no post processing
        else:
            # Carry on experiment
            if start is not None and start0 is not None:
                # ----------------------------------------------
                # Enable the possibility to carry on an experiment
                # run previously from restart files.
                pass
                # ----------------------------------------------

            # ----------------------------------------------
            # Copy required files from self.statdir,
            # Instanciate the model,
            # Update the attributes.
            pass
            # ----------------------------------------------
    
    def read_config(self):
        """Read the configuration file"""
        
        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model for reading
        # a yaml file.
        # If the default version is used, this one must
        # be deleted.
        pass
        # ----------------------------------------------

    def set_time(self, dateref=None, length=None, start=None):
        """Define time of the run
            - dateref: datetime of the start
            - length:  length of the run in seconds
            - start:   initial time
        """
        
        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        pass
        # ----------------------------------------------

    def copy(self, members=None):
        """Create new model instances with the same characteristics
            - members: list of member id
        """

        # ----------------------------------------------
        # Required for Assim Run with ensembles.
        # ----------------------------------------------

        if members is None:
            members = [1]

        listmodels = []                # list of model instances

        # Loop on models
        for m in members:
            self.logger.info('Instanciate member {}'.format(m))
            # ----------------------------------------------
            # New instances of the model with the same characteristics.
            # The new instances must also have the same state.
            # Beware: all the attributes that are updated by external
            #    classes must have the same id (mutable objects)
            #    eg: self.control,
            #        self.interpol (see self.convert_coord())

            # Instanciate new member
            new_instance = NewModel(self.config, self.prm, self.statdir, self.wdir, self.archdir, m)
            # control and interpol must have the same id for both models
            new_instance.control = self.control
            new_instance.interpol = self.interpol
            # Set time
            new_instance.set_time(self.datestart, self.length, self.start)
            # Append the new instance
            listmodels.append(new_instance)
            # ----------------------------------------------
    
        return listmodels

    def get_gps_axis(self):
        """Return a dictionary with the axis for the GP sampler perturbations"""

        # ----------------------------------------------
        # Required for Assim Run with ensembles.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Construct a dictionary for all variables
        # that can be controlled by the assimilation with
        # perturbations generated from the GP sampler
        # gps_axis = { var: np.array(axis) }
        pass
        # ----------------------------------------------

    def __call__(self, dateref=None, length=None, dumptime=None,
                 file=None, overlap=0):
        """Run the model
            - dateref:  datetime of the start
            - start:       start in seconds of the cycle
            - length:   length of the run in seconds
            - dumptime: list of time to output
            - file:     specific file name for logging
            - overlap:  time to output for overlapping windows
         """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------
        
        # Set the times for the run
        self.set_time(dateref, length, self.start)
        
        # Initialise state 
        # ----------------------------------------------
        # Initialise the model depending on what 
        # has been set in self.prepare_next():
        # from a restart file, saved state, ...
        # ----------------------------------------------
        
        # Take into account possible changes from assimilation
        self.init_changes()

        # Run the model
        if length > 0:
            # ----------------------------------------------
            # Possibly manage files according to the step
            # that is run: bkg or rean,
            # Run the model
            pass
            # ----------------------------------------------

    def init_changes(self):
        """Initialise the changes"""

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        if self.state_change is not None:

            # ----------------------------------------------
            # Initialise changes set in the dictionary
            # state_change for each variable.
            for var in self.state_change:
                if var == 'a':
                    pass
            # ----------------------------------------------

            self.state_change = None

    def convert_coord(self, coord, model_coord):
        """Convert observation coordinates into coordinates understandable by the model
           Define the interpolation parameters
            - coord:  dictionary { 'spatial': {coord_type1: [], coord_type2: []},
                                   'time': [datetime] }
                       eg. {'spatial': {('lat','lon'): [(45.,200.)],
                                        ('s',): [36500., 45000.]},
                            'time': [datetime(2018,4,16,0,0,0]}
            - model_coord: dictionary {'spatial': {},
                                       'time': [1800.]}
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------
               
        # ----------------------------------------------
        # Each coordinate of the 'coords' dictionary
        # must be transformed into a coordinate
        # understandable by the model, and stored
        # in 'model_coord'.
        # ----------------------------------------------

        # Loop on coordinate type
        for ctype in coord['spatial']:

            if ctype not in model_coord['spatial']:
                model_coord['spatial'][ctype] = []

            if ctype == ('s',):
                # Example on a coordinate 's' already understandable by the model
                model_coord['spatial'][('s',)] = coord['spatial'][('s',)]

        # ----------------------------------------------
        # It is recommended to define the interpolation 
        # parameters here. Beware, the interpolation 
        # parameters must be stored in a mutable object
        # eg self.interpol = {model_coord_1: { 'ind1': (index, weight),
        #                                      'ind2': (index, weight)}}
        # ----------------------------------------------
    
        # ----------------------------------------------
        # Time coordinate of the 'coords' dictionary
        # must be transformed into a coordinate
        # understandable by the model, and stored
        # in 'model_coord', or possibly shifted to
        # a multiple of the model time step as shown
        # in the example hereafter.
        # ----------------------------------------------

        # Define time at the next multiple of model time step
        for t, tsec in enumerate(model_coord['time']):
            # Multiple of model time step
            tts = int(tsec / int(self.step)) * int(self.step)
            if tsec % int(self.step) > 0:
                tts += int(self.step)
            tts += self.start
            model_coord['time'][t] = tts

    def get_coord_limits(self):
        """Return the limits of the domain"""

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        return []

    def get_Mxb_Hxb(self, datestart, length, file, dumptime, xb_start, obsinfo, pickle=False):
        """Return the background state of the control variables
            at the different time following dumptime and its
            counterpart in observation space
            - datestart:   datetime of the start of the run
            - start:       start in seconds of the cycle
            - length:      length of the run
            - file:        logging results in a specific file
            - dumptime:    list of time to dump
            - xb_start:    get xb at the start (end) of the run (bool)
            - obsinfo:     dictionary of observation information
            - pickle:      save and return state at the end of the length
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------

        # Initialisation
        xb = None
        Mxb = []
        Hxb = []

        # Get xb at the start of the run
        if xb_start:
            # ----------------------------------------------
            # Take into account any possible change
            # and get the control vector at the start of 
            # the current window.
            # ----------------------------------------------
            self.init_changes()
            xb = self.control_vector()

        # Run the model
        self(dateref=datestart, length=length, dumptime=dumptime, file=file)

        # Get xb at the end of the run
        if not xb_start and dumptime[-1] < self.stop:
            # ----------------------------------------------
            # Get the control vector at the end of the
            # current window. Beware that the last time
            # in dumptime is not necessary the end of
            # the current window.
            # ----------------------------------------------
            xb = self.control_vector()

        # ----------------------------------------------
        # For each time in dumptime, append in Mxb the
        # control vector and in Hxb its counterpart in
        # the observation space.
        # ----------------------------------------------

        # Start and stop indices for observations
        obslist = [i for i in range(len(obsinfo['time'])) if obsinfo['time'][i] in dumptime]

        # Loop on time
        for cnt, t in enumerate(dumptime):

            # Control vector
            Mxb.append(self.control_vector())

            # Apply H
            indlist = [i for i in obslist if obsinfo['time'][i] == t]
            Hxb.extend(self.apply_Hnolin(obsinfo, indlist[0], indlist[-1] + 1).array)

        # ----------------------------------------------
        # Possibly change the output files for bkg.
        if file[:3] == 'bkg':
            # Output file
            try:
                file = '{}.{}'.format(file, self.files['output'].split('.')[-1])
                shutil.move(os.path.join(self.outdir, self.files['output']),
                            os.path.join(self.outdir, file))
            except(KeyError, TypeError, IOError):
                pass
        # ----------------------------------------------

        # ----------------------------------------------
        # When the model is run in parallel, pickle all
        # the information that need to be recovered.
        # ----------------------------------------------

        # Save the state at the end of the window
        if pickle:
            gherkins = self.pickle()
        else:
            gherkins = []

        return Vector(xb), Vector(Mxb), Vector(Hxb), gherkins
        
    def control_vector(self):
        """Construct the control vector"""
        
        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------
               
        ctl = np.zeros(self.ctl_length)
        for i, v in enumerate(self.control):
            ind0 = self.first_node[i]
            ind1 = self.first_node[i+1]
            # ----------------------------------------------
            # Set the control vector slice ctl[ind0:ind1]
            # depending on the nature of the control variable.
            if v == 'a':
                ctl[ind0:ind1] = 1
            # ----------------------------------------------
        
        return ctl

    def apply_Hnolin(self, coords, ist, iend):
        """Return the vector in observation space
            Non-linear version of H
            - coords:      dictionary of coordinates after self.convert_coord()
            - ist:         first index to take into account
            - iend:        last index to take into account
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------
               
        # Initialisation
        Hv = np.zeros(iend - ist)

        # Loop on observation type
        listtype = set(coords['obs_type'])
        for obs in listtype:

            # Define observation elements for this variable
            indlist = [i for i in range(ist, iend) if coords['obs_type'][i] == obs]
            hvlist = list(np.array(indlist) - ist)

            # ----------------------------------------------
            # Maps the control vector in the observation space
            # depending on the type of observation, e.g. 'T'.
            if obs == 'T':
                Hv[hvlist] = 1
            # ----------------------------------------------

            else:
                msg1 = 'Non-linear H cannot be applied for observation type {}'.format(obs)
                self.logger.error(msg1)
                raise NewModelError(msg1)

        return Vector(Hv)

    def get_H(self, coords, ist, iend, xb=None, Hxb=None):
        """Return the linear matrix H
            - coords:      dictionary of coordinates after self.convert_coord()
            - ist:         first index to take into account
            - iend:        last index to take into account
            - xb:          Vector of ensemble of xb anomalies in model space
            - Hxb:         Vector of ensemble of anomalies in observation space
                           It is assumed that both xb and Hxb have as many
                           members as parameters and CL in control vector
        """
    
        # ----------------------------------------------
        # Required for Kalman Filter.
        # ----------------------------------------------
               
        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Construct the linear matrix H that maps
        # the model space into the observation space.
        # ----------------------------------------------
        H = np.zeros((iend-ist, self.ctl_length))
        return Matrix(H)

    def check_increment(self, bkg, increment):
        """Check the correctness of the increment
            - bkg:       background state
            - increment: increment to be added to the background state
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # Initialisation
        analysis = bkg + increment
        new_increment = Vector(increment.array)

        for v, var in enumerate(self.control):
            ind0 = self.first_node[v]
            ind1 = self.first_node[v + 1]

            # ----------------------------------------------
            # Check that the analysis is within the bounds
            # defined or consistent.
            # If not, modify the new_increment.
            if var == 'a':
                for i, anl in enumerate(analysis[ind0:ind1].array):
                    # Check min a
                    if anl < self.parameter['limitations']['a_mini']:
                        new_increment[ind0 + i] = self.parameter['limitations']['a_mini'] - bkg[ind0 + i]
                    # Check max a
                    elif anl > self.parameter['limitations']['a_maxi']:
                        new_increment[ind0 + i] = self.parameter['limitations']['a_maxi'] - bkg[ind0 + i]
            # ----------------------------------------------

        if not np.all(new_increment == increment):
            self.logger.warning('check increment: the increment has been modified.')

        return new_increment

    def check_range(self, var, therange, index=None):
        """Check if a range of values is consistent for a variable
            - var:       name of variable
            - therange:     range to check [min,max]
            - index:     index of the variable for BC
        """
        
        # ----------------------------------------------
        # Required for Assim Run with ensembles.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        vmin = therange[0]
        vmax = therange[1]
        rg = vmax - vmin

        # ----------------------------------------------
        # Check that the range of value for a particular
        # variable is consistent.
        # If not, modify the range values.
        if var == 'a':
            if vmin < self.parameter['limitations']['a_mini']:
                vmin = self.parameter['limitations']['a_mini']
                vmax = vmin + rg
            elif vmax > self.parameter['limitations']['a_maxi']:
                vmax = self.parameter['limitations']['a_maxi']
                vmin = vmax - rg
        # ----------------------------------------------

        return [vmin, vmax]
    
    def set_changes(self, bkg=None, increment=None, state=None, values=None, extra=None):
        """Set the changes to take into account for the next run
            - bkg:       background state
            - increment: increment to be added to the background state
            - state:     list of dictionary of changes
            - values:    list of values for changes
            - extra:     list of extra values for changes
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------
               
        # ----------------------------------------------
        # Construct the dictionary of changes to be applied
        # for each variable var
        # self.state_change = { var: { 'index': index for var,
        #                              'value': new value for var at this index
        #                              'inc': True if the values represent an increment}}
        # When perturbations of type GP sampler are used
        # for a variable, the changes can be set this way
        # self.state_change = { var: { 'index': index for var,
        #                              'control': new value for var at this index
        #                              'inc': True if the values represent an increment
        #                              'value': actual perturbation given by extra}}
        # In this case, flags can be used to update the
        # actual perturbations of the Perturbation instance
        # if needed, by appending the required information.
        # ----------------------------------------------

        flags = []

        # Take corrections from increment
        if increment is not None:
            self.state_change = {}

            for v, var in enumerate(self.control):
                ind0 = self.first_node[v]
                ind1 = self.first_node[v + 1]

                # ----------------------------------------------
                # Prepare the increment to be taken into account
                # for each controlled variable at its index.
                # Possibly, the increment can be added to the
                # provided background state to take directly into
                # account the analysis.
                if var == 'a':
                    # Example for a simple variable
                    if bkg is None:
                        for i, index in enumerate(self.control[var]):
                            self.state_change[var] = {'index': index, 'inc': True,
                                                      'value': increment[ind0:ind1].array}
                    else:
                        for i, index in enumerate(self.control[var]):
                            self.state_change[var] = {'index': index, 'inc': False,
                                                      'value': (bkg[ind0:ind1] + increment[ind0:ind1]).array}
                elif var == 'b':
                    # Example for GP sampler hyperparameters
                    st = ind0
                    end = ind0
                    if bkg is None:
                        for i, index in enumerate(self.control[var]):
                            end += len(self.keep_gps[i])
                            self.state_change[var].append({'index': index, 'value': extra[v],
                                                           'inc': True, 'control': increment[st:end].array})
                            st = end
                    else:
                        for i, index in enumerate(self.control[var]):
                            end += len(self.keep_gps[i])
                            self.state_change[var].append({'index': index, 'value': extra[i], 'inc': False,
                                                           'control': (increment[st:end] + bkg[st:end]).array})
                            st = end
                # ----------------------------------------------
            
        if state is not None and values is not None:
            if self.state_change is None:
                self.state_change = {}
            for v, dico in enumerate(state):
                if dico['variable'] not in self.state_change:
                    self.state_change[dico['variable']] = []
                # ----------------------------------------------
                # Prepare to take into account some changes
                # describe in the dictionary state to generate
                # an ensemble.
                if dico['variable'] == 'a':
                    # Example for a simple variable
                    self.state_change['a'] = values[v]
                elif dico['variable'] == 'b':
                    # Example for GP sampler hyperparameters
                    self.state_change['b'].append({'index': dico['index'], 'value': extra[v],
                                                   'inc': False, 'control': values[v]})
                # ----------------------------------------------
            
        # ----------------------------------------------
        # In the case of GP sampler hyperparameters,
        # it is likely that the actual perturbation
        # generated by the hyperparameters have not
        # been checked.
        # This can be done here. In case of modification,
        # the new values of the actual perturbations
        # will be taken into account in the Perturbation
        # instance by adding the required information to flags
        # ----------------------------------------------

        # Check GP sampler perturbation if changes from state or increment
        try:
            for i, change in enumerate(self.state_change['b']):
                flags.append(None)
                # ----------------------------------------------
                # Check the actual perturbation and take into
                # modifications following the example hereafter.
                self.state_change['b'][i]['value'] = 1
                flags[-1] = {'index': change['index'], 'value': self.state_change['b'][i]['value']}
        except KeyError:
            pass

        return flags

    def update_gps(self, extra):
        """Update the GP sampler perturbation reference
            - extra:  increment to add
        """

        # ----------------------------------------------
        # Required for Assim Run with ensembles.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Update the GP sampler perturbation reference.
        pass
        # ----------------------------------------------

    def prepare_next_run(self, directory=True, save_state=False, restartdir=None,
                         restore_state=False, dump_state=False, state=False,
                         persist=False, time=True, shift=0, init_now=False):
        """Prepare next run
            - directory:   prepare directories
            - save_state:  save current state
            - restartdir:  directory containing the restart file
            - restore_state: restore state saved previously
            - dump_state:  dump state saved previously
            - state:       initialise from a particular state
            - persist:     persist boundary conditions for forecast
            - time:        set time of the run
            - shift:       shift of the time in seconds
            - init_now:    initialise the changes now
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------
               
        # Empty output and figures directories
        if directory:
            shutil.rmtree(self.outdir)
            shutil.rmtree(self.figdir)
            os.makedirs(self.outdir)
            os.makedirs(self.figdir)
            
        # Save current state
        if save_state:
            # ----------------------------------------------
            # Save the current state, parameters and trends.
            # Ignored if there is no cycling.
            pass
            # ----------------------------------------------
        
        # Dump state previously stored
        if dump_state:
            # ----------------------------------------------
            # Dump the state, parameters and trends saved previously.
            # Ignored if there is not cycling.
            pass
            # ----------------------------------------------

        # Initial condition
        if restartdir is not None:
            # ----------------------------------------------
            # Initialise the run from the restart files
            # of the directory restartdir.
            pass
            # ----------------------------------------------
        else:
            # Restore state
            if restore_state:
                # ----------------------------------------------
                # Initialise the run from a previously saved
                # state, parameters and trends.
                # Ignored if there is not cycling.
                pass
                # ----------------------------------------------
            elif time and shift != 0:
                # ----------------------------------------------
                # Initialise the run from a state, parameters
                # and trends saved for the overlap.
                # Ignored if there is not cycling.
                pass
                # ----------------------------------------------
            elif state:
                # ----------------------------------------------
                # Initialise the run from a particular state.
                # Ignored if there is not cycling.
                pass
                # ----------------------------------------------

        # Model times
        if time:
            self.set_time(dateref=self.datestop + timedelta(seconds=shift), start=self.stop + shift)

        # Boundary conditions
        if persist:
            # ----------------------------------------------
            # Save the current parameters and boundary
            # conditions before persisting them
            # for a forecast run.
            # Set an attribute to mention the persisting
            # mode e.g. self.persist = True
            # and use it to reinitilise the parameters
            # and boundary conditions after the forecast.
            # Ignored if forecast cannot be run.
            pass
            # ----------------------------------------------
        # elif self.persist:
            # Renitialise the boundary conditions
         
        # Initialise now
        if init_now:
            self(dateref=self.datestart, length=0)

    def move_outputs(self, paths):
        """Move outputs into archiving directory
           - paths:     dictionary with the paths for archiving
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # Move the outputs from self.outdir and self.figdir
        # into paths['run'].
        # If self.member > 0, the output files must be
        # renamed with the member id
        # eg output.log  -->  output_memb5.log  for member 5.
        # ----------------------------------------------

        # Move outputs
        try:
            pass
        except IOError as err:
            msg1 = 'Cannot move the output files:\n{}'.format(err)
            self.logger.error(msg1)
            raise NewModelError(msg1)
        
    def dump_restart(self, restartdir):
        """Dump restart files
            - restartdir:  directory containing the restart files
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # Filename
        thedate = self.datestart.strftime('%Y%m%d-%H%M')

        # ----------------------------------------------
        # Create restart files from the current state.
        # Move the restart files into the directory restartdir.
        # The restart file names must include the date
        # of validity thedate
        # eg restart.out  -->  20180516-1300_restart.out.
        # If self.member > 0, the restart files must be 
        # renamed with the member id
        # eg 20180516-1300_restart_memb5.out.
        # ----------------------------------------------
    
    def dump_assim_restart(self, restartdir):
        """Dump restart information for assimilation
            - restartdir:  directory containing the restart files
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Create a restart file with assimilation information
        # required to carry on the run if needed.
        # The restart file is named as shown hereafter.
        # ----------------------------------------------

        # Filename
        thedate = self.datestart.strftime('%Y%m%d-%H%M')
        filename = '{}/{}_assim_model.txt'.format(restartdir, thedate)

    def pickle(self, time_only=False, overlap=False):
        """Pickle for parallelisation
            - time_only: pickle the time only
            - overlap:   True for pickling state for sliding window
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # When the different model instanciations run in
        # parallel, any information at the end of the run
        # must be pickled to go back to serial.
        # The option time_only is used by Experiment to
        # save information on the time.
        pass
        # ----------------------------------------------

    def unpickle(self, gherkins, time_only=False, overlap=False):
        """Unpickle for parallelisation
            - gherkins:  Saved information
            - time_only: Unpickle the time only
            - overlap:   True for pickling state for sliding window
        """

        # ----------------------------------------------
        # Required
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Restore the informations saved by self.pickle().
        # The option time_only is used by Experiment to
        # restore information on the time.
        pass
        # ----------------------------------------------

    def reconstruct_output_file(self, filelist):
        """ Reconstruct output files by keeping the last state for each time step
            - filelist:    list of files for the reconstruction
        """

        # ----------------------------------------------
        # Required for Assim Run.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # When the assimilation window is split in
        # different cycles, there is generally one
        # output file per cycle. Those different files
        # listed in filelist, must be concatenated in
        # one single file. If the last time step of a file
        # corresponds to the first time step of the next
        # file, the last time step must be discarded.
        #
        # It is possible to not reconstruct a single
        # reanalysis file. In this case however, the
        # method self.move_outputs() must be written
        # accordingly.
        pass
        # ----------------------------------------------

    def extract_info(self, location, variable, datedir, times, size_ensemble, fname=None):
        """Find the indices for output file extraction
            - location:       list of model-like coordinates (tuple),
                              or 'all' for all locations. If tuple, take the closest location.
            - variable:       list of variables to extract
            - datedir:        cycling directory
            - times:          list of time to extract in seconds. Take the closest time.
            - size_ensemble:  size of the ensemble
            - fname:          name of file, default given by the instance configuration
        """

        # ----------------------------------------------
        # Required for Post processing.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Model.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # When ensemble methods are used, it can be useful
        # to store information on indices to find a particular
        # variable and a particular location in an output
        # file, such that this information is defined once
        # for all the members and the possible cycles.
        #
        # For example self.var_ind can be used to log
        # the lines or columns where to find the variables
        # in the list variable. Other examples can be
        # self.loc_ind for location, and self.tim_ind
        # for times.
        pass
        # ----------------------------------------------

    def extract_from_file(self, location, variable, datedir, times, size_ensemble, fname=None):
        """Extract a variable at a particular location for post processing
            - location:       list of model-like coordinates (tuple),
                              or 'all' for all locations
            - variable:       list of variables to extract
            - datedir:        cycling directory
            - times:          list of time to extract in seconds
            - size_ensemble:  size of the ensemble
            - fname:          name of file, default given by the instance configuration
        """

        # ----------------------------------------------
        # Required for Post processing.
        # ----------------------------------------------

        # Initialisation
        res = {}

        # ----------------------------------------------
        # Prepare a list of files to be open. When the
        # data to be extracted are from the background
        # state, fname is provided and define the start
        # of the name given by the assimilation.
        # Ensemble members must be taken into account as well.

        # List of file names
        if size_ensemble == 1:
            if fname is None:
                filenames = ['{}/{}/toto.txt'.format(self.archdir, datedir)]
            else:
                filenames = ['{}/{}/{}_toto.txt'.format(self.archdir, datedir, fname)]
        else:
            if fname is None:
                filenames = ['{}/{}/toto_memb{}.txt'.format(self.archdir, datedir, m)
                             for m in range(1, size_ensemble + 1)]
            else:
                filenames = ['{}/{}/{}_toto_memb{}.txt'.format(self.archdir, datedir, fname, m)
                                for m in range(1, size_ensemble + 1)]
        # ----------------------------------------------

        # ----------------------------------------------
        # Extract data from file.
        # Return a dictionary with data:
        # if location == 'all':
        #     { variable: { 'all': {time: np.array((size_ensemble, size of model)) }}}
        # else:
        #     { variable: { location: { time: np.array(size_ensemble) }}}

        # Loop on filenames
        for f, file in enumerate(filenames):

            # Loop on variables
            for var in variable:
                res[var] = {}
        # ----------------------------------------------

    def __repr__(self):
        """Information"""
        
        # ----------------------------------------------
        # A default version exists in Model.
        # However, it is better to update the name 
        # and parameters.
        pass
        # ----------------------------------------------
