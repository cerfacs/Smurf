"""
NewInstrument
=============

Instrument: NewInstrument

"""

# ----------------------------------------------
# Required: Replace NewInstrument by the name of
# the new instrument class everywhere in the file.
# ----------------------------------------------
import logging
import numpy as np
import os
import csv
from .instrument import Instrument
# ----------------------------------------------
# Required: add an error class in
# ../common.errors.py
# ----------------------------------------------
from ..common.errors import NewInstrumentError


# ----------------------------------------------
# Affect a number Id to the new instrument in
# the class Instrument
# instrument.py
#
# Define the new instrument as a package so it
# can be used on its own as well as in Smurf
# __init__.py
#
# Add the nex instrument in the instanciation method
# instanciate.py
# ----------------------------------------------

# ======================================================


class NewInstrument(Instrument):

    logger = logging.getLogger('NewInstrument')
    logging.Logger.setLevel(logger, logging.INFO)

    # =================================== #
    #               Methods               #
    # =================================== #

    def __init__(self, config, prm, dateref=None, size=1):
        """Constructor
            - config:     dictionary of configuration
            - prm:        model parameters
            - dateref:    datetime of the cycle
            - size:       size of the ensemble for omb
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # Basic initialisation
        self.name = 'NewInstrument'
        Instrument.__init__(self, config, prm, dateref, size)

        # ----------------------------------------------
        # Initialise specific attributes.
        # ----------------------------------------------

        # Specific attributes
        # self.loc = ['Loc1', 'Loc2']
        # self.index_loc = {'Loc1: np.empty(0, dtype=np.int8), 'Loc2: np.empty(0, dtype=np.int8)}

        # ----------------------------------------------
        # Possibly initialise coordinates
        # if better values can be defined now:
        # self.spatial_coord, self.dim, self.tcoord.
        # Possibly define the spatial coordinate themselves
        # if required and known.
        # ----------------------------------------------

        # Coordinates
        # self.spatial_coord = ('lat', 'lon')
        # self.dim = 2
        # self.spcoord = { Loc1: (45.0, 3.0), Loc2: (40.0, 4.0) }
        # or
        # self.lat = np.empty(0, dtype=np.float32)
        # self.lon = np.empty(0, dtype=np.float32)

        # ----------------------------------------------
        # Possibly initialise self.sigma
        # if it is defined in the configuration.
        # ----------------------------------------------

        # Quality
        # self.sigma = self.conf['sigma']

        # ----------------------------------------------
        # Possibly initialise temporary attributes
        # that are used during the initialisation.
        # ----------------------------------------------

        # Temporary attributes
        # self.data

        # Read observation files
        self.read_observation_files()

        # ----------------------------------------------
        # Finalise the initialisation by updating any
        # attributes depending on the observations read.
        # ----------------------------------------------

        # Finalise the initialisation
        self.rejevent = np.zeros(self.nobs)
        self.measure.mask = np.zeros(self.nobs)
        self.omb = np.ma.array(np.zeros((self.size, self.nobs)),
                               mask=np.zeros((self.size, self.nobs)), dtype=np.float32)
        self.nvalobs = self.nobs

        # Reject observations with no value
        self.reject_no_value()

        # Reject observations with poor quality
        self.reject_quality()

        # Selection of observations
        self.reject_not_selected()

    def read_observation_files(self):
        """Read the observation files"""

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # Loop on files:
        for fobs in self.files:

            # ----------------------------------------------
            # Read the observation files
            # Observations can be opened depending on their extension
            # Possibly take into account specific format
            # Example where files are opened depending on their
            # extension before the data are processed by the
            # method (to be written) append_data():

            # Extension of the file
            ext = fobs.split('.')[-1]

            # Check configuration information
            try:
                delimiter = self.conf['format'][ext]['delimiter']
            except (KeyError, TypeError):
                delimiter = ';'
            try:
                time_format = self.conf['format'][ext]['time_format']
            except (KeyError, TypeError):
                time_format = "%d/%m/%y %H:%M"

            # Open file and read data
            if ext == 'csv':
                with open(os.path.join(self.path, fobs), 'r') as fin:
                    self.data = csv.reader(fin, delimiter=delimiter)
            elif ext == 'txt':
                with open(os.path.join(self.path, fobs), 'r') as fin:
                    data = fin.readlines()
                self.data = [line[:-1].split(delimiter) for line in data]
            else:
                msg = 'Extension {} for NewInstrument observation not permitted'.format(ext)
                self.logger.error(msg)
                raise NewInstrumentError(msg)

            # Append the data
            self.append_data(time_format)

            # Keep only the observations required (e.g. specific obs_type)
            # Update the following attributes:
            # self.obs_type, self.index_obs_type,
            # self.tcoord, spatial coordinate,
            # possibly self.quality,
            # self.measure, self.sigma, self.nobs
            # and possibly other specific attributes.
            # ----------------------------------------------

    def get_spatial_coord(self):
        """Return a list of spatial coordinates for not rejected observations"""

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Instrument when
        # no spatial coordinates are defined.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Return a list of observation spatial coordinates
        # used by ObsVector to initialise the transformation
        # into model-compliant coordinates.
        # It is better if the list contains unique values.
        # Examples:
        coords = [self.spcoord[loc] for loc in self.loc if np.ma.count(self.measure[self.index_loc[loc]]) > 0]
        # or
        coords = [(lat, lon) for i, lat, lon in zip(range(self.nobs), self.lat, self.lon) if self.rejevent[i] == 0]
        return list(set(coords))
        # ----------------------------------------------

    def get_time_coord(self):
        """Return a list of time coordinates for not rejected observations"""

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Instrument.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Return a list of observation time coordinates
        # used by ObsVector to initialise the transformation
        # into model-compliant coordinates.
        # It is better if the list contains unique values.
        pass
        # ----------------------------------------------

    def get_observations(self, otype, listtime):
        """Return observations of type otype at time t
            - otype:       type of observation
            - listtime:    list of datetime
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # Send the valid observations and the information:
        #    list of the valid observation indices
        #    list of the valid observation spatial coordinates
        #    list of the valid observation measurement
        #    list of the valid observation standard deviation
        # Example:
        ind = []
        for t in listtime:
            ind.extend(np.intersect1d(self.index_obs_type[otype], np.where(self.tcoord == t)[0]))
        indval = [i for i in ind if self.rejevent[i] == 0]
        measure = self.measure[indval]

        location = [list(filter(lambda d: (i in d[1]), self.index_loc.items()))[0][0] for i in indval]
        spcoord = [self.spcoord[loc] for loc in location]
        sigma = [self.sigma[loc][otype] for loc in location]
        # or
        spcoord = [(self.lat[i], self.lon[i]) for i in indval]
        sigma = self.sigma[indval]

        return indval, spcoord, measure, sigma
        # ----------------------------------------------

    def set_omb(self, d, ind):
        """Set obs minus bkg values
            - d:    innovation vector, possibly an ensemble vector
            - ind:  indices of observations
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Instrument.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Log the innovations for each of this instrument
        # observations in order to write a report.
        pass
        # ----------------------------------------------

    def reject_not_selected(self):
        """Reject observations that do not correspond to the selection"""

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Instrument.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Observations can be selected in the configuration.
        pass
        # ----------------------------------------------

    def reject_domain_out(self, limits, convert_coord):
        """Reject observations out of domain
            limits:          limits of domain
            convert_coord:   dictionary for coordinate conversion {'original': [], 'model': []}
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # A default version exists in Instrument.
        # If the default version is used, this one must
        # be deleted.
        # ----------------------------------------------

        # ----------------------------------------------
        # Define the indices of observations whose
        # spatial coordinates are outside the limits.
        # The limits of the domain are model-like coordinates
        # The dictionary convert_coord gives a list of original
        # coordinates and a list of their correspondence in
        # model-like coordinates
        pass
        # ----------------------------------------------

    def write_observations(self, fname, obs, reftime, timeline, loc_name=None):
        """Write an observation file
            - fname:    file name without extension
            - obs:      dictionary of observations { variable: { location: { 0: np.array( len(timeline) ),
                                                                             'sigma': sigma }}}
            - reftime:  datetime of the reference time
            - timeline: time of the observation  in seconds since reftime
            - loc_name: name of location
        """

        # ----------------------------------------------
        # Required.
        # ----------------------------------------------

        # ----------------------------------------------
        # Write observations at the specific format
        # of the instrument
        pass
        # ----------------------------------------------

    def __repr__(self):
        """Information"""

        # ----------------------------------------------
        # A default version exists in Instrument
        # However, it is better to update the name
        # and parameters.
        pass
        # ----------------------------------------------
