This test proposes to run Mascaret on a 50-kilometre reach of the Garonne
(South of France) from Tonneins to La Réole, via Marmande. **It is advised to copy
the whole directory Mascaret in a place where Mascaret can actually be run.**

##### 1) Directory Garonne:

The configuration for Mascaret can be found in the directory Garonne.
Two different upstream discharge time series are available:

  * Hydro_Tonneins_1981_original.loi --> reference
  * Hydro_Tonneins_1981_modified.loi --> reference + noise

One of them must be chosen and linked under  
  * Hydro_Tonneins_1981.loi

The friction coefficients can be set up in the file:

  * config_Garonne.json

For the reference, the true values are

  * {"type": "zone", "index": 0, "value": 40},
  * {"type": "zone", "index": 1, "value": 32},
  * {"type": "zone", "index": 2, "value": 33}

For the control (without assimilation) and assimilation experiments, the noisy
values are:

  * {"type": "zone", "index": 0, "value": 30},
  * {"type": "zone", "index": 1, "value": 40},
  * {"type": "zone", "index": 2, "value": 40}


##### 2) Directory Observations:

Hourly observations of water height are available at Mas d'Agenais and Marmande.
These observations are synthetic, constructed from a reference run.

##### 3) Running twin experiments:

The experiments are run with a 2-day spinup plus 12 hours to be assessed.
Except for the Reference experiment, the simulation is cycled with
3-hourly windows with (Assim) or without (Control) assimilation. At the
end of each window, a 12-hour forecast is launched. Then the window is slided
of 1 hour and a new 3-hourly window starts. With the current settings, the experiment begin on 19991228-00:00 and restart files are written every 6 hours to continue the experiement. Synthetic observations are generated at Mas d'Agenais and Marmande.
The experiments are launched by the command:

   * python tests.py

###### i. Reference experiment:

  * configrun.yml:
     * name: Reference
     * type: Free Run
  * Use the true values of the friction coefficients in Garonne/config_Garonne.yml
  * Link Hydro_Tonneins_1981_original.loi to Hydro_Tonneins_1981.loi in Garonne

Comments:
* The Spin up run and the Free run for the Reference are integrated as a single and continuous simulation
* Directory /Archive/Reference/init contains all input files for the Reference run.
* 3 restarts (.lig) are written in /Archive/Reference/restart: beginning of the Spin up (19991228-00:00), end of the Spin Up (19991230-00:00) and end of the experiment (19991230-12:00).
* Output directories are organised by dates. The dates indicated the beginning of the simulation and contains directories spinup and/or reanalysis. /Archive/Reference/19991228-0000/spinup (that contains the .opt file for the 2-day spin up period) and /Archive/Reference/19991230-0000/reanalysis (that contains the .opt file for the 12-hour reanalysis period), along with associated .lig files. The output frequency for .opt files is set in .xcas file (here 30 min).

###### ii. Construct observations:

Observations can be constructed from the reference run in /Archive/Reference/post-processing/obs/Observations.csv run by launching

  * python construct_obs.py

and then moved to appropriate directory:

  * cp Archive/Reference/post_processing/obs/Observations.csv Observations/Observations_Garonne.csv

This file contains the instrument code (0 here for gauge), the instrument type for each station, the 3 columns (time, h at Mas d'Agenais, h at Marmande)

###### iii. Control experiment:

  * configrun.yml:
     * name: Control
     * type: Dry Run
  * Use the noisy values of the friction coefficients in Garonne/config_Garonne.yml
  * Link Hydro_Tonneins_1981_modified.loi to Hydro_Tonneins_1981.loi in Garonne.

Output directories are organised by dates. The dates indicated the beginning of the simulation and contains directories spinup and/or reanalysis and/or forecast. There is NO ASSIMILATION for the deterministic Control run, but the run is cycled, like in assimilation mode with sliding windows.
* /Archive/Control/19991228-0000/spinup that contains the .opt file for the 2-day spin up period
* /Archive/Control/19991230-0000 to /Archive/Control/19991230-0900 contain a /reanalysis directory with .opt file for the 3-hour reanalysis period, along with associated .lig files. The output frequency for .opt files is set in .xcas file (here 30 min). Directory /Archive/Control/19991230-0000 contains .opt for simulation from 00:00 (taht is the time for the beginning of the data assimilation experiment) to 03:00. Directory /Archive/Control/19991230-0900 contains .opt for simulation from 09:00 to 12:00 which is the final time of the experiment.
* /Archive/Control/19991230-0300 to /Archive/Control/19991230-1200 contain a /forecast directory with .opt file for the 12-hour forecast period, along with associated .lig files. The ous tput frequency for .opt files is set in .xcas file (here 30 min). Directory /Archive/Control/19991230-0300 contains .opt for forecast from 03:00 to 15:00; this run is integrated during the FIRST "assimilation" cycle, that starts at 00:00, "assimilated" observations op to 03:00 and then starts the forecast at 03:00. Directory /Archive/Control/19991230-1200 contains .opt for forecast from 12:00 to 24:00;  this run is integrated during the LAST "assimilation" cycle, that starts at 09:00, "assimilated" observations up to 12:00 and then starts the forecast at 12:00.
* For each cycle, for the deterministic control run, the hydraulic state is saved as a snapshot, for further use as the IC for the following cycle (1 hour latter).

###### iv. Assimilation experiment:

  * configrun.yml:
     * name: Assim
     * type: Assim Run
  * Use the noisy values of the friction coefficients in Garonne/config_Garonne.yml
  * Link Hydro_Tonneins_1981_modified.loi to Hydro_Tonneins_1981.loi in Garonne directory.

Assimilation settings are given in config_EnKF.yml file. The control vector is described ('control' key-word), as well as the location of the observation ('stations' key-word): The assimlation corrects the three Strickler coefficients and hyperparametres of the
upstream discharge time series (batman-ot Gaussian process sampler) by assimilating synthetical
hourly observations at Mas d'Agenais and Marmande. The ensemble has 12 members.

Key-word 'smoother' indicates if the re-analysis is restarted from the beginning of the assimilation window ('true') or if the simulation is restarted from the end of the assimilation window ('false').

Key-word 'seed' indicates if the ensemble is generated at each cycle with 'any' (using the mean of the analysis, but initial variance) or not with 'once' if the ensemble is ONLY generated at the beginning of the FIRST assimilation cycle (no further sampling of uncertainties in the following cycles). When 'any', the cycling uses previously stored snapshots as IC, computed from previously analysed control, but used for the current cycle with control values that stems for the current cycle uncertainty sampling; there is some inconsistency here.

Output directories are organised by dates. The dates indicated the beginning of the simulation and contains directories spinup and/or reanalysis and/or forecast.
* /Archive/Assim/19991228-0000/spinup that contains the .opt file for the 2-day spin up period
* /Archive/Assim/19991230-0000 to /Archive/Assim/19991230-0900 contain a /reanalysis directory with .opt file for the 3-hour reanalysis period, along with associated .lig files. The output frequency for .opt files is set in .xcas file (here 30 min). Directory /Archive/Control/19991230-0000 contains .opt for simulation from 00:00 (taht is the time for the beginning of the data assimilation experiment) to 03:00. Directory /Archive/Control/19991230-0900 contains .opt for simulation from 09:00 to 12:00 which is the final time of the experiment.
* /Archive/Assim/19991230-0000 to /Archive/Assim/19991230-0900 contain a /step1 directory with files related to the 3-h assimilation analysis. Files .lig and .opt for each member, files increment_membreXX.txt with values of the control increment for each member, as well as the covariance matrix covariance.pic are stored in sub-directory /Archive/Assim/datexxxxxxxx-xxxx/assim. File obs_assimilated.txt is stored in /Archive/Obs/datexxxxxxxx-xxxx/assim and described how many observations are used in the cycle, the obs type, location, time, variance, value and OmB for each member.
* /Archive/Assim/19991230-0300 to /Archive/Assim/19991230-1200 contain a /forecast directory with .opt file for the 12-hour forecast period, along with associated .lig files. The output frequency for .opt files is set in .xcas file (here 30 min). Directory /Archive/Control/19991230-0300 contains .opt for forecast from 03:00 to 15:00; this run is integrated during the FIRST assimilation cycle, that starts at 00:00, assimilated observations op to 03:00 and then starts the forecast at 03:00. Directory /Archive/Control/19991230-1200 contains .opt for forecast from 12:00 to 24:00;  this run is integrated during the LAST assimilation cycle, that starts at 09:00, assimilated observations up to 12:00 and then starts the forecast at 12:00.
* For each cycle, for the deterministic control run, the hydraulic state is saved as a snapshot, for further use as the IC for the following cycle (1 hour latter).

###### v. Post processing:

After the three experiments have been run, some postprocessing can be launched
by the command:

   * python Garonne/postproc_Garonne.py

Name of Reference, Control and Assim experiments, variable of interest and observation station are given in postproc.py.
Post-processing outputs are stored in /Archive/Assim/post-pro. Proposed post-processing for Mascaret experiment are here :
* Water heiht at a given location for a chosen lead time along cycles

![h_Marmande_Assim](Plots/h_Marmande_Assim.png)


* Water elevation at a given date (in re-analysis mode or in forecast mode for chosen lead time) along the river.

![z_19991230-1200_Assim](Plots/z_19991230-1200_Assim.png)

* Increment for one of the control element along the analysis cycles
 
![Increment_Ks2](Plots/Increment_Ks2.png)

* Covariance matrices: norm of A and B along cyles or matrix at a given cycle
 
![Correlation](Plots/Correlation.png)

![covariance_norm](Plots/covariance_norm.png)

* Innovations: Mean and rms for OmB along cycles and rank diagram for OmB (for 1 cycle or over all experiement ??)
 
![h_innovations](Plots/h_innovations.png)

![rank_all_obs](Plots/rank_all_obs.png)

* OSSE specific: RMSE between Control/Reference and Assim/Reference along cycles for re-analysis time or chosen lead-time computed over the whole the hydraulic network. RMSE between Control/Reference and Assim/Reference along the hydraulic network computed over time or lead-time.

![rmse_h_space](Plots/rmse_h_space.png)

![rmse_h_time](Plots/rmse_h_time.png)

![fcst_h](Plots/fcst_h.png)
