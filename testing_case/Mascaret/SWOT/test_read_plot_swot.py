import matplotlib.pyplot as plt
import netCDF4
import numpy as np
from datetime import datetime, timedelta


# read nc file
fulldata = netCDF4.Dataset('rivertile_run0_pass0060.nc','r')
print (fulldata)

# read node data
data = fulldata['nodes']
lons = data['longitude'][:]
lons = np.array(lons)
lons[np.where(lons > 180)] -= 360.
lats=data['latitude'][:]
heights=data['height'][:]
node_id = data['node_id'][:]
nb_node = data.dimensions['nodes'].size
# time, same for all nodes
tmp = data['time'][:]
tmp = [datetime(2000, 1, 1) + timedelta(seconds=int(tmp[t])) for t in range(nb_node)]
tcoord = []
tcoord = np.append(tcoord, tmp)
# plot
myplot = plt.scatter(lons,lats,c=heights,cmap="jet",marker='.')
plt.colorbar(myplot, orientation = 'horizontal')
plt.xlabel('longitude')
plt.ylabel('latitude')
plt.title('Water elevation at river node at time %s' %str(tcoord[0]))
plt.savefig('test_read_plot_swot_nodes.png',format='png')
plt.show()
plt.close()

# read reach data
data = fulldata['reaches']
lons = data['p_longitud'][:]
lons = np.array(lons)
lons[np.where(lons > 180)] -= 360.
lats=data['p_latitud'][:]
heights=data['height'][:]
reach_id = data['reach_id'][:]
nb_reach = data.dimensions['reaches'].size
# time, same for all reaches
tmp = data['time'][:]
tmp = [datetime(2000, 1, 1) + timedelta(seconds=int(tmp[t])) for t in range(nb_reach)]
tcoord = []
tcoord = np.append(tcoord, tmp)
# plot
myplot = plt.scatter(lons,lats,c=heights,cmap="jet",marker='.')
plt.colorbar(myplot, orientation = 'horizontal')
plt.xlabel('longitude')
plt.ylabel('latitude')
plt.title('Water elevation at river reach at time %s' %str(tcoord[0]))
plt.savefig('test_read_plot_swot_reaches.png',format='png')
plt.show()
plt.close()

