from Smurf.experiments.experiment import Experiment as Exp
from Smurf.observations.instanciate import instanciate as instance_obs
#from Smurf.observations.instrument import Instrument as Inst
#from Smurf.observations.gauge import Gauge
#from Smurf.observations.swot import Swot

#dico_config = {'instrument': 'Gauge', 'keep': True, 'path': '/home/globc/ricci/DA/Smurf/testing_case/Mascaret/Observations', 'subdir_format': None, 'files': 'Observations_Garonne.csv', 'subname_format': None, 'obs_type': None, 'stations': ['Marmande', "Mas d'Agenais"], 'coord_type': 's', 'coordinate': {"Mas d'Agenais": 21925.0, 'Marmande': 36290.0}, 'zNGF': {"Mas d'Agenais": 0.0, 'Marmande': 0.0}, 'sigma': {"Mas d'Agenais": {'h': 0.1}, 'Marmande': {'h': 0.1}}, 'no_value': 1.0, 'selection': {'frequency': None, 'shift': None}, 'format': {'csv': {'time_format': '%d/%m/%y %H:%M', 'delimiter': ';'}}}


#mygauge = Gauge(config = dico_config, prm = dico_prm, dateref = None, size = 1) 
#myswot = Swot(config = dico_config_swot, prm = dico_prm_swot, dateref = None, size = 1) 



dico_config_swot = {'instrument': 'Swot', 'product': 'river reach', 'process': None, 'keep': True, 'path': '/home/globc/ricci/DA/Smurf/testing_case/Mascaret/SWOT/', 'subdir_format': None, 'files': ['rivertile_run0_pass0060.nc'], 'subname_format': None, 'obs_type': None}


dico_prm = {'variables': {'h': ['water height', 'm'], 'z': ['water elevation', 'm'], 'zb': ['bathymetry elevation', 'm'], 'q': ['discharge', 'm3.s-1'], 'Ksmin': ['Strickler coefficient for minor', 'm1/3.s-1'], 'Ksmaj': ['Strickler coefficient for major', 'm1/3.s-1'], 'bc_cst': ['Constant boundary condition', ['m3.s-1', 'm', 'm3.s-1', 'm']], 'bc_abc': [['homothetic vertical transformation for bc', 'amplitude shift for bc', 'time shift for bc'], ['', 'm3.s-1', 's']], 'bc_gps': ['GP sampler coefficient', '']}, 'composite': {'bc_abc': [['bc_a', 'bc_b', 'bc_c'], [1.0, 0.0, 0.0]]}, 'store_anl': ['Ksmin', 'Ksmaj', 'bc_cst', 'bc_abc', 'bc_gps', 'h', 'q'], 'balance_var': {'h': ['q'], 'q': ['h']}, 'coordinates': {'s': 'curv. abs.', 'zn': 'zone', 'lat': 'latitude', 'lon': 'longitude'}, 'limitations': {'Ks_mini': [10.0, 10.0, 10.0], 'Ks_maxi': [100.0, 100.0, 100.0], 'bc_Q_mini': 0.0, 'bc_Q_maxi': 15000.0, 'bc_H_mini': 0.0, 'bc_H_maxi': 50.0, 'bc_a_mini': 0.1}}


inst_obs = instance_obs(dico_config_swot['instrument'], dico_config_swot, dico_prm, size=1)

inst_obs.plot()
