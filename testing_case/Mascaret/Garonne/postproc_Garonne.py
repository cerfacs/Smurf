from Smurf.postprocessing.postprocessing import PostProcessing as PP
from datetime import datetime
from matplotlib import pyplot as plt
import os
import numpy as np
from scipy import *

var = 'h'
st = 'Marmande'
coords = (36290.0, )
instr = '1'

path = os.getcwd()
name_ref = 'Reference'
name_ctl = 'Control'
name_asm = 'Assim'

# Instanciate the postprocessing for each run
pp_ref = PP(archdir='{}/Archive'.format(path), name=name_ref)
pp_ctl = PP(archdir='{}/Archive'.format(path), name=name_ctl)
pp_asm = PP(archdir='{}/Archive'.format(path), name=name_asm)

# Get assimilated observations from assimilaton output files
# Warning : for a 3h assim window here
obs_assimilated = pp_asm.read_obs(variable=var)
obs_assimilated_value = obs_assimilated[var][instr][str(coords)]['obs']
obs1=np.array(obs_assimilated_value[0][0:-1:3], ndmin= 2)
obs2=np.array(obs_assimilated_value[0][1:-1:3], ndmin= 2)
obs3 = np.array(append(obs_assimilated_value[0][2:-1:3], obs_assimilated_value[0][-1]), ndmin=2)

# Get curvilinear abscissa
refs = pp_ref.extract_field('s', date=pp_asm.exp.start, write=False)
curv_abs = refs['s'][pp_asm.exp.start][0][0, :]
st_tick = ["Mas d'Agenais", "Marmande", 'La Réole']
xticks = [(21925.0,), (36290.0,), (62175.0,)]

# Extract and plot the chronicle of water height at Marmande
print('Water height time series at Marmande')
ref, tline_ref = pp_ref.extract_chronicle(coords, var, shift_start=[3, 'hour'],
                                          frequency=[1, 'hour'], write=False)
ctl, tline_ctl = pp_ctl.extract_chronicle(coords, var, lead_time=[0, 'hour'], write=False) 
asm, tline_asm = pp_asm.extract_chronicle(coords, var, lead_time=[0, 'hour'], write=False)
extract = [asm[var][coords][0], ctl[var][coords][0], ref[var][coords][0], obs1, obs2, obs3]
title = '{} at {}'.format(var, st)
svgname = '{}_{}_{}'.format(var, st, name_asm)
pp_asm.plot_chronicle(var, extract, tline_asm[0], color=['r', 'b', 'k', 'g*', 'g+', 'gx'], ens_color='silver',
                      alpha=0.3, title=title, labels=['asm', 'ctl', 'ref', 'obs1', 'obs2', 'obs3'], svgname=svgname)

# Extract and plot field of water elevation for 30/12/1999 12:00
print('Water elevation along the reach on 30/12/1999 12:00')
fvar = 'z'
date = datetime(1999, 12, 30, 12)
ref = pp_ref.extract_field(fvar, date=date, write=False)
ctl = pp_ctl.extract_field(fvar, date=date, write=False)
asm = pp_asm.extract_field(fvar, date=date, write=False)
field = [asm[fvar][date][0], ctl[fvar][date][0], ref[fvar][date][0]]
title = '{} on {}'.format(fvar, date.strftime('%d/%m/%Y %H:%M'))
svgname = '{}_{}_{}'.format(fvar, date.strftime('%Y%m%d-%H%M'), name_asm)
pp_asm.plot_field(fvar, field, curv_abs, mean_only=True, color=['r', 'b', 'k'], ens_color='silver',
                  alpha=0.3, title=title, labels=['asm', 'ctl', 'ref'], xticks=xticks,
                  xtick_labels=st_tick, svgname=svgname)

# Extract and plot the increment on Ks at index 2
print('Increment for the Strickler coefficient zone 2')
cvar = 'Ksmin'
index = 2
asm, tline = pp_asm.extract_assim(cvar, index, assim_type='inc')
pp_asm.plot_chronicle(cvar, asm[cvar][index][1]['inc'][:, 0, :], tline, color='r', ens_color='silver',
                      alpha=0.3, title='Increment Ks 2', svgname='Increment_Ks2')

# Read covariances
print('Covariance norm and B correlations')
cov, control, tline = pp_asm.read_covariance(cov_type=['B', 'A'])
# Plot norm
pp_asm.plot_covariance_norm(cov, tline, title='Covariance norm', svgname='covariance_norm')
# Plot correlation of last cycle
C, _ = pp_asm.cor_from_cov(cov['B'][1][-1, :, :])
pp_asm.plot_covariance(C, 'B', control=control, cmap='bwr', title='Correlation',
                       vmin=-1, vmax=1, svgname='Correlation')

# Read innovations
print('Innovation statistics and rank diagram')
omb = pp_asm.read_omb(variable=var)
# Compute and plot innovation statistics
omb_stat = pp_asm.omb_statistics(omb)
pp_asm.plot_stat_chronicle(var, omb_stat[var], title='OmB statistics for h', svgname='h_innovations')
# Compute and plot rank diagrams
obs_rank = pp_asm.obs_rank(omb)
pp_asm.plot_rank({'cnt': obs_rank['cnt'], 'rank': obs_rank['rank']}, title='All Observations', svgname='rank_all_obs')

# Twin experiment diagnostics
print('Rmse time series and along the reach with respect to Reference')
cstat = pp_ctl.field_statistics(pp_ref, var)
fstat = pp_asm.field_statistics(pp_ref, var)
tline = cstat[var]['timeline']
extract = [cstat[var]['rms_t'].reshape((1, len(tline))), fstat[var]['rms_t'].reshape((1, len(tline)))]
svgname = 'rmse_{}_time'.format(var)
pp_asm.plot_chronicle(var, extract, tline, color=['b', 'r'], mean_only=True,
                      title='rms error on {}'.format(var), labels=['ctl', 'asm'], svgname=svgname)
dim = cstat[var]['rms_sp'].size
field = [cstat[var]['rms_sp'].reshape((1, dim)), fstat[var]['rms_sp'].reshape((1, dim))]
svgname = 'rmse_{}_space'.format(var)
pp_asm.plot_field(var, field, curv_abs, mean_only=True, color=['b', 'r'], xticks=xticks,
                  title='rms error on {}'.format(var), labels=['ctl', 'asm'], xtick_labels=st_tick, svgname=svgname)

# Twin experiment forecast diagnostics
print('Forecast rmse with respect to Reference for different lead times')
cstatf = [cstat[var]['rms']]
fstatf = [fstat[var]['rms']]
ldlist = range(13)
for i in ldlist[1:]:
    tmp = pp_ctl.field_statistics(pp_ref, var, lead_time=[i, 'hour'])
    cstatf.append(tmp[var]['rms'])
    tmp = pp_asm.field_statistics(pp_ref, var, lead_time=[i, 'hour'])
    fstatf.append(tmp[var]['rms'])
plt.plot(cstatf, 'b', label='ctl')
plt.plot(fstatf, 'r', label='asm')
plt.xticks(ldlist, ['{}h'.format(t) for t in ldlist])
plt.xlabel('Lead time')
plt.ylabel('water height in m')
plt.title('rms error for water height')
plt.legend()
plt.grid()
plt.xlim((ldlist[0], ldlist[-1]))
plt.savefig('{}/plot/fcst_h.png'.format(pp_asm.ppdir), format='png')
plt.show()

