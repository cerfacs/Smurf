from Smurf.postprocessing.postprocessing import PostProcessing as PP
import os

var = ['h']
st = ["Mas d'Agenais", 'Marmande']
coords = [(21925.0, ), (36290.0,)]
frequency = [1, 'hour']
std = [[0.1], [0.1]]

path = os.getcwd()
name_ref = 'Reference'

# Instanciate the postprocessing
pp_ref = PP(archdir='{}/Archive'.format(path), name=name_ref)
pp_ref.construct_observation(coords, var, std, frequency=frequency, location_name=st,
                             svgname='Observations', instrument='Gauge')
