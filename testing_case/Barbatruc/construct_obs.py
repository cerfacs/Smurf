from Smurf.postprocessing.postprocessing import PostProcessing as PP
import os
import itertools

var = ['ps']
loc = [(i, j) for i, j in itertools.product(range(10, 40, 10), range(10, 40, 10))]
frequency = [40, 'sec']           # Milli seconds in reality
std = [[1.e-5]] * len(loc)

path = os.getcwd()
name_ref = 'Reference'

# Instanciate the postprocessing
pp_ref = PP(archdir='{}/Archive'.format(path), name=name_ref)
pp_ref.construct_observation(loc, var, std, frequency=frequency, shift_start=[50, 'sec'],
                             location_name=loc, svgname='Observations', instrument='Barbametre')
