#### I - Introduction:

This test proposes to run Barbatruc, a toy model solving the two-dimensional
Navier-Stokes equations developed by Antoine Dauptain at Cerfacs.
Barbatruc is the 12th step of the 12 steps to Navier-Stokes from Professor Lorena Barba:

http://nbviewer.jupyter.org/github/barbagroup/CFDPython/blob/master/lessons/15_Step_12.ipynb

A squared domain is defined (40 x 40) with a source (passive scalar = 1.) at a 
particular point (10, 10). The source can be blurred with a Shapiro filter. 
The parameters rho (density) and nu (kinematic viscosity) are set up, as well 
as a source term to mimic the effect of pressure-driven channel (u-west). The 
Navier-Stokes equations are integrated until t_end (0.1 s). The horizontal components 
of the velocity (u and v), the pressure (p) and the passive tracer (ps) fields 
are outputed every 100 time steps and at the final time. The corresponding times 
are also recorded. 

#### II - Running twin experiments:

From the same reference run and set of observations, two test cases can be launched:

   * Case 1: correction of the parameters rho and nu, and the source term u_west
   * Case 2: correction of the initial condition, i.e. the magnitude and position 
   of the source
   
The experiments are run without any cycling and are launched by the command:

   * python test.py
   
*Smurf does not yet manage times smaller than 1 second. In the configuration files,
times are written in seconds but are actually milliseconds.*

##### 1) Reference and observations

###### i. Reference experiment:

  * configrun.yml:
     * name: Reference
     * type: Free Run
  * config_case.yml:
     * rho: 1.0
     * nu: 1.0
     * u_west: 20.0
     * source.location: [10, 10]
     * source.value: 1.0
 
###### ii. Construct observations:

Observations every 10 grid points at time 0.04 s and 0.09 s can be constructed 
from the reference run by launching

  * python construct_obs.py

and then moved to appropriate directory:

  * cp Archive/Reference/post_processing/obs/Observations.csv Observations/.


##### 2) Control and assimilation for case 1

###### i. Control experiment:

  * configrun.yml:
     * name: Control_case_1
     * type: Free Run
  * config_case.yml:
     * rho: 5
     * nu: 0.5
     * u_west: 10.0
     * source.location: [10, 10]
     * source.value: 1.0
  
###### ii. Assimilation experiment:

  * configrun.yml:
     * experiment.name: Assim_case_1
     * experiment.type: Assim Run
     * assim.step1.config: config_EnKF_case_1.yml
  * config_case.yml:
     * rho: 5
     * nu: 0.5
     * u_west: 10.0
     * source.location: [10, 10]
     * source.value: 1.0
  
The ensemble has 12 members that run simultaneously in parallel on 4 processors.

##### 2) Control and assimilation for case 2

###### i. Control experiment:

  * configrun.yml:
     * name: Control_case_2
     * type: Free Run
  * config_case.yml:
     * rho: 1.0
     * nu: 1.0
     * u_west: 20.0
     * source.location: [12, 8]
     * source.value: 2.0
  
###### ii. Assimilation experiment:

  * configrun.yml:
     * experiment.name: Assim_case_2
     * experiment.type: Assim Run
     * assim.step1.config: config_EnKF_case_2.yml
  * config_case.yml:
     * rho: 1.0
     * nu: 1.0
     * u_west: 20.0
     * source.location: [12, 8]
     * source.value: 2.0
  
The ensemble has 12 members that run simultaneously in parallel on 4 processors.

##### 3) Post processing
 
After the Reference, Control and Assim experiments have been run, some postprocessing 
can be launched by the command:

   * python postproc.py

The names of the control and assim experiments must be set up in the postproc.py 
file before launching:
   * For case 1:
     * name_ctl = 'Control_case_1'
     * name_asm = 'Assim_case_1'
   * For case 2:
     * name_ctl = 'Control_case_2'
     * name_asm = 'Assim_case_2'

