from Smurf.postprocessing.postprocessing import PostProcessing as PP
from Smurf.common import sec2unit
from matplotlib import pyplot as plt
import os
import numpy as np
from datetime import timedelta
import itertools

var = 'ps'

path = os.getcwd()
name_ref = 'Reference'
name_ctl = 'Control_case_2'
name_asm = 'Assim_case_2'

# Instanciate the postprocessing for each run
pp_ref = PP(archdir='{}/Archive'.format(path), name=name_ref)
pp_ctl = PP(archdir='{}/Archive'.format(path), name=name_ctl)
pp_asm = PP(archdir='{}/Archive'.format(path), name=name_asm)

# Read obs locations
omb = pp_asm.read_omb(variable=var)
loc = sorted(list(omb[var]['30'].keys()))
xo = [int(eval(ll)[0]) for ll in loc]
yo = [int(eval(ll)[1]) for ll in loc]

# Extract and plot fields 
print('Passive tracer fields for reference and control run')
ps_ref = pp_ref.extract_field(var, date=None)
ps_ctl = pp_ctl.extract_field(var, date=None)
cl = plt.imshow(ps_ref[var][pp_asm.exp.start+timedelta(seconds=pp_asm.exp.reanalysis)][0][0,:,:] * 1.e+5, cmap='magma', vmin=0, vmax=10)
plt.scatter(xo, yo, marker='x', cmap='jet', c=[2]*9, vmin=1, vmax=3)
plt.title('Reference for passive scalar x 10^-5')
plt.colorbar(cl)
plt.savefig('{}/plot/reference_psfield.png'.format(pp_asm.ppdir), format='png')
plt.show()

cl = plt.imshow(ps_ctl[var][pp_asm.exp.start+timedelta(seconds=pp_asm.exp.reanalysis)][0][0,:,:] * 1.e+5, cmap='magma', vmin=0, vmax=10)
plt.scatter(xo, yo, marker='x', cmap='jet', c=[2]*9, vmin=1, vmax=3)
plt.title('Control for passive scalar x 10^-5')
plt.colorbar(cl)
plt.savefig('{}/plot/control_psfield.png'.format(pp_asm.ppdir), format='png')
plt.show()

# Extract and plot the increment for the parameters
print('Increment for the parameters')
var_asm = []
if name_asm[-1] == '1':
    var_asm.extend(['rho', 'nu', 'u_west'])
elif name_asm[-1] == '2':
    var_asm.extend(['source', 'sloc_y', 'sloc_x'])
asm, _ = pp_asm.extract_assim(var_asm, [1]*len(var_asm), assim_type=['bkg', 'anl'])

for v in var_asm:
    bkg = asm[v][1][1]['bkg'][:, 0, 0]
    anl = asm[v][1][1]['anl'][:, 0, 0]
    if v == 'sloc_y' or v == 'sloc_x':
        anl = np.round(anl)
    n, bins, patch = plt.hist([bkg, anl], bins=5, density=True, color=['b', 'r'], label=['bkg', 'anl'])
    plt.title('Histogram for {}'.format(v))
    plt.legend()
    plt.savefig('{}/plot/{}.png'.format(pp_asm.ppdir, v), format='png')
    plt.show()

# Read covariances
print('Covariance norm and B correlations')
cov, control, _ = pp_asm.read_covariance(cov_type=['B', 'A'])
normb = pp_asm.cov_norm(cov['B'][1][0, :, :])
norma = pp_asm.cov_norm(cov['A'][1][0, :, :])
print('||B|| = {:.03f} --> ||A|| = {:.03f} ({:.02f}%)'.
      format(normb, norma, 100. * (norma - normb) / normb))
# Plot correlation
C, _ = pp_asm.cor_from_cov(cov['B'][1][0, :, :])
pp_asm.plot_covariance(C, 'B', control=control, cmap='bwr', title='Correlation',
                       vmin=-1, vmax=1, svgname='Correlation')

# Compute and plot innovation statistics
print('Innovation statistics and rank diagram')
omb_stat = pp_asm.omb_statistics(omb)
rms = np.ma.array(np.zeros((pp_asm.exp.model.ny, pp_asm.exp.model.ny)),
                  mask=np.ones((pp_asm.exp.model.ny, pp_asm.exp.model.ny)))
for ll in loc:
    ind = (int(eval(ll)[0]), int(eval(ll)[1]))
    rms.mask[ind] = False
    rms[ind] = omb_stat[var]['30'][ll]['rms']
cl = plt.imshow(rms * 1.e+5, cmap='magma_r', vmin=0, vmax=25)
plt.title('Innovation rms for passive scalar x 10^-5')
plt.colorbar(cl)
plt.savefig('{}/plot/innovations_rms.png'.format(pp_asm.ppdir), format='png')
plt.show()

# Compute OmA
tlist = []
for ll in loc:
    tlist.extend(omb[var]['30'][ll]['date'])
tlist = list(set(tlist))
ps_bkg = pp_asm.extract_field(var, date=tlist, bkg=True)
ps_anl = pp_asm.extract_field(var, date=tlist)
oma = {var: {'30': {}}}
for ll in loc:
    oma[var]['30'][ll] = {}
    oma[var]['30'][ll]['date'] = omb[var]['30'][ll]['date']
    oma[var]['30'][ll]['timeline'] = omb[var]['30'][ll]['timeline']
    oma[var]['30'][ll]['omb'] = omb[var]['30'][ll]['omb'].copy()
    ind = (int(eval(ll)[0]), int(eval(ll)[1]))
    for t, dd in enumerate(tlist):
        ps_b = ps_bkg[var][dd][0]
        ps_a = ps_anl[var][dd][0]
        oma[var]['30'][ll]['omb'][:,t] += ps_b[:, ind[0], ind[1]] - ps_a[:, ind[0], ind[1]]
oma_stat = pp_asm.omb_statistics(oma)
rms_a = np.ma.array(np.zeros((pp_asm.exp.model.ny, pp_asm.exp.model.ny)),
                    mask=np.ones((pp_asm.exp.model.ny, pp_asm.exp.model.ny)))
for ll in loc:
    ind = (int(eval(ll)[0]), int(eval(ll)[1]))
    rms_a.mask[ind] = False
    rms_a[ind] = oma_stat[var]['30'][ll]['rms']
cl = plt.imshow(rms_a * 1.e+5, cmap='magma_r', vmin=0, vmax=25)
plt.title('OmA rms for passive scalar x 10^-5')
plt.colorbar(cl)
plt.savefig('{}/plot/oma_rms.png'.format(pp_asm.ppdir), format='png')
plt.show()

# Compute and plot rank diagrams
obs_rank = pp_asm.obs_rank(omb)
pp_asm.plot_rank({'cnt': obs_rank['cnt'], 'rank': obs_rank['rank']}, title='All Observations', svgname='rank_all_obs')

# Twin experiment diagnostics
print('Rmse time series and along the reach with respect to Reference')
cstat = pp_ctl.field_statistics(pp_ref, var, shift_start=sec2unit(pp_asm.exp.model.t_end))
fstat = pp_asm.field_statistics(pp_ref, var)
cl = plt.imshow(cstat[var]['rms_sp'] * 1.e+5, cmap='magma', vmin=0, vmax=5)
plt.scatter(xo, yo, marker='x', cmap='jet', c=[2]*9, vmin=1, vmax=3)
plt.title('Control rmse wrt Reference for passive scalar x 10^-5')
plt.colorbar(cl)
plt.savefig('{}/plot/control_rms.png'.format(pp_asm.ppdir), format='png')
plt.show()
cl = plt.imshow(fstat[var]['rms_sp'] * 1.e+5, cmap='magma', vmin=0, vmax=5)
plt.scatter(xo, yo, marker='x', cmap='jet', c=[2]*9, vmin=1, vmax=3)
plt.title('Assim rmse wrt Reference for passive scalar x 10^-5')
plt.colorbar(cl)
plt.savefig('{}/plot/assim_rms.png'.format(pp_asm.ppdir), format='png')
plt.show()
