"""Rejection events"""

# Definition of rejection events 
events = {0: 'accepted',
          1: 'no measurement',
          2: 'out of domain',
          3: 'out of time window',
          4: 'poor quality',
          5: 'QC failed',
          6: 'not selected'}
