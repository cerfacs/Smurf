"""
SWOT altimetry
=============

Instrument: Swot

"""

import logging
import os
import numpy as np
import netCDF4
from .instrument import Instrument
from ..common.errors import SwotError
from ..common.functions import sec2unit

# ======================================================


class Swot(Instrument):

    logger = logging.getLogger('Swot')
    logging.Logger.setLevel(logger, logging.INFO)

    # =================================== #
    #               Methods               #
    # =================================== #

    def __init__(self, config, prm, dateref=None, size=1):
        """Constructor
            - config:     dictionary of configuration
            - prm:        model parameters
            - dateref:    datetime of the cycle
            - size:       size of the ensemble for omb
        """

        # Basic initialisation
        self.name = 'Swot'
        Instrument.__init__(self, config, prm, dateref, size)

        # For postprocessing purpose
        if config == {}:
            return

        # Coordinates
        self.spatial_coord = ('lat', 'lon')                  # Coordinate type
        self.spatial_dim = 2                                 # Dimension of coordinates
        self.lat = np.empty(0, dtype=np.float32)             # Latitude array
        self.lon = np.empty(0, dtype=np.float32)             # Longitude array

        # Observations
        try:
            self.process = self.conf['process']              # Pre-processing of the observations
        except (KeyError, TypeError):
            self.process = None
        self.sigma = np.ma.empty(0, dtype=np.float32)        # Error standard deviation

        # Temporary attributes
        self.data = None                                     # Data read from file

        # Read observation files
        self.read_observation_files()

        # Finalise the initialisation
        self.obs_type = ['z']
        self.index_obs_type = {'z': np.array(range(self.nobs), dtype=np.int8)}
        self.rejevent = np.zeros(self.nobs, dtype=np.int8)
        self.measure.mask = np.zeros(self.nobs)
        self.omb = np.ma.array(np.zeros((self.size, self.nobs)),
                               mask=np.zeros((self.size, self.nobs)), dtype=np.float32)
        self.nvalobs = self.nobs

        # Reject observations with no value
        self.reject_no_value()

        # Reject observations with poor quality
        self.reject_quality()

        # Selection of observations
        self.reject_not_selected()

    def read_observation_files(self):
        """Read the observation files"""

        # Loop on files:
        for fobs in self.files:
            try:
                self.data = netCDF4.Dataset(os.path.join(self.path, fobs), 'r')
            except IOError:
                msg = 'The observation file {} does not exist in the path\n{}'.format(fobs, self.path)
                self.logger.error(msg)
                raise SwotError(msg)
            else:
                self.append_data()

    def append_data(self):
        """Append pixel cloud observations"""

        msg = 'The Swot method append_data is not implemented'
        raise NotImplementedError(msg)

    def get_spatial_coord(self):
        """Return a list of spatial coordinates for not rejected observations"""

        return [(lat, lon) for lat, lon in zip(self.lat, self.lon)]

    def __repr__(self):
        """Information"""

        string = 'Swot: \n'
        if self.keep:
            string += '   Keep:                          {}\n'.format(self.keep)
        else:
            string += '   Cycling date:                  {}\n'.format(self.dateref.strftime("%d/%m/%Y %H:%M"))
        string += '   Type:                          {}\n'.format(self.obs_type)
        if self.frequency > 0:
            val, unit = sec2unit(self.frequency)
            val2, unit2 = sec2unit(self.shift)
            string += '   Selection:                     frequency: {} {}, shift: {} {}\n'.\
                format(val, unit, val2, unit2)
        else:
            string += '   Selection:                     All observations \n'
        string += '   Minimum quality:               {}\n'.format(self.quality_min)
        string += '   Number of observations:        {}\n'.format(self.nobs)
        string += '   Number of valid observations:  {}\n'.format(self.nvalobs)
        return string
