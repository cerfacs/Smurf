"""
SWOT altimetry for river reach products
=======================================

Instrument: Swot
Product: River Reach

"""

import logging
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.colors as plc
from .swot import Swot
from ..common.functions import sec2unit

# ======================================================


class SwotRiverReach(Swot):

    logger = logging.getLogger('Swot')
    logging.Logger.setLevel(logger, logging.INFO)

    # =================================== #
    #               Methods               #
    # =================================== #

    def __init__(self, config, prm, dateref=None, size=1):
        """Constructor
            - config:     dictionary of configuration
            - prm:        model parameters
            - dateref:    datetime of the cycle
            - size:       size of the ensemble for omb
        """

        # Attributes for river reach
        self.nb_reach = 0                                    # Number of reaches
        self.index_reach = {}                                # Indices of observations related to a reach
        self.reach_id = []                                   # Reach id format CBBBBBRRRNNNT

        # Basic initialisation
        Swot.__init__(self, config, prm, dateref, size)

        # For postprocessing purpose
        if config == {}:
            return

        # Finalise the initialisation
        self.nb_reach = len(self.reach_id)

    def append_data(self):
        """Append river reach observations"""

        self.data = self.data['reaches']

        # Reaches information and coordinates
        nb_reach = self.data.dimensions['reaches'].size
        reach_id = self.data['reach_id'][:]
        for i, rid in enumerate(reach_id):
            if rid not in self.reach_id:
                self.reach_id.append(rid)
                self.lat = np.append(self.lat, self.data['p_latitud'][i])
                self.lon = np.append(self.lon, self.data['p_longitud'][i])
                self.index_reach[rid] = []
            self.index_reach[rid].append(self.nobs + i)

        # Time coordinate
        tmp = self.data['time'][:]
        tmp = [datetime(2000, 1, 1) + timedelta(seconds=int(tmp[t])) for t in range(nb_reach)]
        self.tcoord = np.append(self.tcoord, tmp)

        # Water level
        self.measure = np.append(self.measure, self.data['height'][:])
        self.no_value = self.data['height'].getncattr('_FillValue')

        # Uncertainty
        self.sigma = np.append(self.sigma, self.data['height_u'][:])

        # Quality
        self.quality = np.append(self.quality, self.data['reach_q'][:])

        # Number of observations
        self.nobs += nb_reach

    def get_observations(self, otype, listtime):
        """Return observations of type otype at time t
            - otype:       type of observation
            - listtime:    list of datetime
        """

        if otype == self.obs_type[0]:

            ind = []
            for t in listtime:
                ind.extend(np.where(self.tcoord == t)[0])
            indval = [i for i in ind if self.rejevent[i] == 0]
            reach_id = [list(filter(lambda d: (i in d[1]), self.index_reach.items()))[0][0] for i in indval]
            reach_ind = [self.reach_id.index(r) for r in reach_id]
            spcoord = [(self.lat[i], self.lon[i]) for i in reach_ind]

            return indval, spcoord, self.measure[indval], self.sigma[indval]

        else:
            return [], [], np.empty(0, dtype=np.float32), np.empty(0, dtype=np.float32)

    def reject_domain_out(self, limits, convert_coord):
        """Reject observations out of domain
            limits:          limits of domain
            convert_coord:   dictionary for coordinate conversion {'original': [], 'model': []}
        """

        # Find out of domain coordinates
        # -----------------------------
        # Mascaret like for the moment
        # -----------------------------
        model = np.array([f[0] for f in convert_coord['model']])
        mini = limits[0][0]
        maxi = limits[1][0]
        ind = np.union1d(np.where(model < mini)[0], np.where(model > maxi)[0])

        # Reject the observations for these coordinates
        spcoord = [convert_coord['original'][i] for i in ind]
        ind_rej = []
        for sc in spcoord:
            try:
                reach_id = self.reach_id[np.union1d(np.where(self.lat == sc[0])[0], np.where(self.lon == sc[1])[0])[0]]
                ind_rej.extend(self.index_reach[reach_id])
            except IndexError:
                pass
        self.reject(ind_rej, 'out of domain')

    def plot(self):

        marker = '.'
        cmap = 'jet'
        # vmin = np.floor(np.min(self.measure))
        vmin = 0.
        # vmax = np.ceil(np.max(self.measure))
        vmax = 30.
        lon = np.array(self.lon)
        lon[np.where(lon > 180)] -= 360.
        cs = plt.scatter(lon, self.lat, c=self.measure, marker=marker, cmap=cmap, norm=plc.Normalize(vmin, vmax, True))
        plt.colorbar(cs, orientation='horizontal')
        plt.xlabel('Longitude')
        plt.ylabel('Latitude')
        plt.title('River Reach Water Elevation')
        plt.savefig('river_reach.png', format='png')
        plt.show()
    
    def __repr__(self):
        """Information"""

        string = 'Swot: \n'
        if self.keep:
            string += '   Keep:                          {}\n'.format(self.keep)
        else:
            string += '   Cycling date:                  {}\n'.format(self.dateref.strftime("%d/%m/%Y %H:%M"))
        string += '   Type:                          {}\n'.format(self.obs_type)
        if self.frequency > 0:
            val, unit = sec2unit(self.frequency)
            val2, unit2 = sec2unit(self.shift)
            string += '   Selection:                     frequency: {} {}, shift: {} {}\n'.\
                format(val, unit, val2, unit2)
        else:
            string += '   Selection:                     All observations \n'
        string += '   Product:                       river reach\n'
        string += '   Minimum quality:               {}\n'.format(self.quality_min)
        string += '   Number of observations:        {}\n'.format(self.nobs)
        string += '   Number of valid observations:  {}\n'.format(self.nvalobs)
        string += '   Number of reaches:             {}\n'.format(self.nb_reach)
        return string
